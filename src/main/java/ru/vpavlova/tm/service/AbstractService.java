package ru.vpavlova.tm.service;

import ru.vpavlova.tm.api.IRepository;
import ru.vpavlova.tm.api.IService;
import ru.vpavlova.tm.entity.AbstractEntity;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public E add(final E entity) {
        if (entity == null) throw new ObjectNotFoundException();
        return repository.add(entity);
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) throw new ObjectNotFoundException();
        repository.remove(entity);
    }

}
