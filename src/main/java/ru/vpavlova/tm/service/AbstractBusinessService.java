package ru.vpavlova.tm.service;

import ru.vpavlova.tm.api.IBusinessRepository;
import ru.vpavlova.tm.api.IBusinessService;
import ru.vpavlova.tm.entity.AbstractBusinessEntity;
import ru.vpavlova.tm.enumerated.Status;
import ru.vpavlova.tm.exception.empty.EmptyIdException;
import ru.vpavlova.tm.exception.empty.EmptyNameException;
import ru.vpavlova.tm.exception.empty.EmptyUserIdException;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;
import ru.vpavlova.tm.exception.system.IndexIncorrectException;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    private final IBusinessRepository<E> repository;

    public AbstractBusinessService(IBusinessRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    public List<E> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.findAll(userId);
    }

    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (comparator == null) return null;
        return repository.findAll(userId, comparator);
    }

    public E add(final String userId, final E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new ObjectNotFoundException();
        return repository.add(userId, entity);
    }

    public E findById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    public E findByIndex(final String userId, Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.findByIndex(userId, index);
    }

    public E findByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.findByName(userId, name);
    }

    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.clear(userId);
    }

    public E updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    public E updateById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findById(userId, id);
        if (entity == null) return null;
        entity.setId(id);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    public E startByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    public E startById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final E entity = findById(userId, id);
        if (entity == null) return null;
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    public E startByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findByName(userId, name);
        if (entity == null) return null;
        entity.setStatus(Status.IN_PROGRESS);
        return entity;
    }

    public E finishByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        entity.setStatus(Status.COMPLETE);
        return entity;
    }

    public E finishById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final E entity = findById(userId, id);
        if (entity == null) return null;
        entity.setStatus(Status.COMPLETE);
        return entity;
    }

    public E finishByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findByName(userId, name);
        if (entity == null) return null;
        entity.setStatus(Status.COMPLETE);
        return entity;
    }

    public E changeStatusByIndex(final String userId, final Integer index, final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final E entity = findByIndex(userId, index);
        if (entity == null) return null;
        entity.setStatus(status);
        return entity;
    }

    public E changeStatusById(final String userId, final String id, final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final E entity = findById(userId, id);
        if (entity == null) return null;
        entity.setStatus(status);
        return entity;
    }

    public E changeStatusByName(final String userId, final String name, final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findByName(userId, name);
        if (entity == null) return null;
        entity.setStatus(status);
        return entity;
    }

    public void remove(final String userId, final E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new ObjectNotFoundException();
        repository.remove(userId, entity);
    }

    public E removeById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(userId, id);
    }

    public E removeByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        return repository.removeByIndex(userId, index);
    }

    public E removeByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.removeByName(userId, name);
    }

}

