package ru.vpavlova.tm.repository;

import ru.vpavlova.tm.api.repository.ITaskRepository;
import ru.vpavlova.tm.entity.Task;
import ru.vpavlova.tm.exception.entity.TaskNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final List<Task> taskList = new ArrayList<>();
        for (final Task task : entities) if (projectId.equals(task.getProjectId())) taskList.add(task);
        return taskList;
    }

    @Override
    public void removeAllByProjectId(final String userId, final String projectId) {
        entities.removeIf(task -> projectId.equals(task.getProjectId()));
    }

    @Override
    public Task bindTaskToProject(final String userId, final String taskId, final String projectId) {
        final Task task = findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(final String userId, final String taskId) {
        final Task task = findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }

}
