package ru.vpavlova.tm.repository;

import ru.vpavlova.tm.api.repository.IUserRepository;
import ru.vpavlova.tm.entity.User;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new ObjectNotFoundException();
        for (final User user: entities) {
            if (user == null) continue;
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user : entities) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (final User user : entities) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) throw new ObjectNotFoundException();
        entities.remove(user);
        return user;
    }

}
