package ru.vpavlova.tm.api;

import ru.vpavlova.tm.entity.AbstractBusinessEntity;
import ru.vpavlova.tm.enumerated.Status;

import java.util.Comparator;
import java.util.List;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IService<E> {

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    E add(String userId, E entity);

    E findById(String userId, String id);

    E findByIndex(String userId, Integer index);

    E findByName(String userId, String name);

    void clear(String userId);

    E updateByIndex(String userId, Integer index, String name, String description);

    E updateById(String userId, String id, String name, String description);

    E startByIndex(String userId, Integer index);

    E startById(String userId, String id);

    E startByName(String userId, String name);

    E finishByIndex(String userId, Integer index);

    E finishById(String userId, String id);

    E finishByName(String userId, String name);

    E changeStatusByIndex(String userId, Integer index, Status status);

    E changeStatusById(String userId, String id, Status status);

    E changeStatusByName(String userId, String name, Status status);

    void remove(String userId, E entity);

    E removeById(String userId, String id);

    E removeByIndex(String userId, Integer index);

    E removeByName(String userId, String name);

}
