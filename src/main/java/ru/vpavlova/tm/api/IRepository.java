package ru.vpavlova.tm.api;

import ru.vpavlova.tm.entity.AbstractEntity;

public interface IRepository<E extends AbstractEntity> {

    E add(E entity);

    void remove(E entity);

}
