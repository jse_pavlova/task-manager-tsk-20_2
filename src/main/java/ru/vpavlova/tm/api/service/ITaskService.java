package ru.vpavlova.tm.api.service;

import ru.vpavlova.tm.api.IBusinessService;
import ru.vpavlova.tm.entity.Task;

public interface ITaskService extends IBusinessService<Task> {

    Task add(String userId, String name, String description);

}
